<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
/**
 * Class : Admin_Get_Post
 *
 * @since  1.0.0
 * @access public
 */
if ( ! class_exists( 'Admin_Get_Post' ) ) :
    class Admin_Get_Post {

		/**
         * Add Custom Metabox
         *
         * @since  1.0.0
         * @access public
         * @return void
         */
		public function wgp_custom_meta_boxes(){
			add_meta_box( 
		        'wpg-metabox',
		        __( 'Primary Category', 'wpwgp' ),
		        array( $this, 'wpg_render_metabox' ),
		        'post',
		        'normal',
		        'default'
		    );
		}

		/**
         * Display Metabox HTML
         *
         * @since  1.0.0
         * @access public
         * @return void
         */
		public function wpg_render_metabox(){
			// Check file availability
			if( file_exists( wp_get_post()->admin_dir.'forms/wp-get-post-form.php' )){
				include_once( wp_get_post()->admin_dir.'forms/wp-get-post-form.php' );
			}
		}

    	/**
         * Loads functions
         *
         * @since  1.0.0
         * @access public
         * @return void
         */
    	public function wgp_save_post( $post_id, $post ){
    		//Validate Post 
    		if ( empty( $post_id ) || empty( $post ) || empty( $_POST ) ) return;
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;
			if ( is_int( wp_is_post_revision( $post ) ) ) return;
			if ( is_int( wp_is_post_autosave( $post ) ) ) return;
			if ( empty($_POST['_wgp_nonce_field']) || ! wp_verify_nonce( $_POST['_wgp_nonce_field'], '_wgp_nonce_action' ) ) return;
			if ( ! current_user_can( 'edit_post', $post_id ) ) return;
			if ( $post->post_type != 'post' ) return;

			$primary_category = isset( $_POST['_wgp_primary_'.$post->post_type] ) ? $_POST['_wgp_primary_'.$post->post_type] : ""; 
			
			// Update Post metadata_exists( $meta_type, $object_id, $meta_key )
			update_post_meta( $post_id, '_wgp_primary_'.$post->post_type, $primary_category );
    		
    	}

    	/**
         * Loads functions
         *
         * @since  1.0.0
         * @access public
         * @return void
         */
    	public function admin_hook(){
    		
			// Add admin Metabox
			add_action( 'add_meta_boxes', array( $this, 'wgp_custom_meta_boxes' ), 10, 2 );
			// Save POST Meta
			add_action( 'save_post', array( $this, 'wgp_save_post' ), 10, 2 );
			
	   	}
    }
endif;



