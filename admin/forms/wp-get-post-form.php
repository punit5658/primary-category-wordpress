<?php
/**
 * Get Post from Primary category
 * @since 1.0.0
 */

// Exit if accessed directly 
if ( !defined( 'ABSPATH' ) ) exit;
?>
<!-- . begining of wrap -->
<div class="wrap">
  <!-- beginning of the plugin options form -->
    <?php
    global $post;
    // Get Post Meta
    $wpg_primary_category = get_post_meta( $post->ID, '_wgp_primary_'.$post->post_type, true );

    ?>
    <?php wp_nonce_field( '_wgp_nonce_action', '_wgp_nonce_field' ); ?>
    <!-- beginning of the settings meta box --> 
    <table class="form-table">
        <tr>
          <th class="row"><?php _e( 'Select Primary Category', 'wpwgp'); ?></th>
          <?php 
              $args = array(
                          'name' => '_wgp_primary_'.$post->post_type,
                          'hide_empty' => 0,
                          'selected' => $wpg_primary_category
                      );
          ?>
          <td><?php wp_dropdown_categories( $args ); ?><br>
          <p class="description"><?php _e( 'Please select primary category.', 'wpwgp' ) ?></p></td>
        </tr>
    </table>
  <!-- end of the settings meta box -->   
</div><!-- .end of wrap -->