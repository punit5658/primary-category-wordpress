<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 * Plugin Name: WP Get POST
 * Plugin URI:  http://punit.info
 * Description: Ability to get post from primary category
 * Version:     1.0.0
 * Author:      Punit
 * Author URI:  http://punit.info/
 * Text Domain: wpwgp
 * Domain Path: /languages
 */

/**
 * Class : Wp_Get_Post
 *
 * @since  1.0.0
 * @access public
 */
if ( ! class_exists( 'Wp_Get_Post' ) ) :
	class Wp_Get_Post {

		/**
		 * Plugin directory path.
		 *
		 * @since  1.0.0
		 * @access public
		 * @var    string
		 */
		public $dir_path = '';

		/**
		 * Plugin directory URI.
		 *
		 * @since  1.0.0
		 * @access public
		 * @var    string
		 */
		public $dir_uri = '';

		/**
		 * Plugin admin directory path.
		 *
		 * @since  1.0.0
		 * @access public
		 * @var    string
		 */
		public $admin_dir = '';

		/**
		 * Plugin includes directory path.
		 *
		 * @since  1.0.0
		 * @access public
		 * @var    string
		 */
		public $inc_dir = '';

		/**
		 * Plugin CSS directory URI.
		 *
		 * @since  1.0.0
		 * @access public
		 * @var    string
		 */
		public $css_uri = '';

		/**
		 * Plugin JS directory URI.
		 *
		 * @since  1.0.0
		 * @access public
		 * @var    string
		 */
		public $js_uri = '';

		/**
		 * Returns the instance.
		 *
		 * @since  1.0.0
		 * @access public
		 * @return object
		 */
		public static function get_instance() {

			static $instance = null;

			if ( is_null( $instance ) ) {
				$instance = new Wp_Get_Post();
				$instance->setup();
				$instance->includes();
			}

			return $instance;
		}

		/**
		 * Sets up globals.
		 *
		 * @since  1.0.0
		 * @access private
		 * @return void
		 */
		private function setup() {

			// Main plugin directory path and URI.
			$this->dir_path = trailingslashit( plugin_dir_path( __FILE__ ) );
			$this->dir_uri  = trailingslashit( plugin_dir_url( __FILE__ ) );

			// Plugin directory paths.
			$this->inc_dir   = trailingslashit( $this->dir_path . 'inc' );
			$this->admin_dir = trailingslashit( $this->dir_path . 'admin' );

			// Plugin Version
			$this->version = '1.0.0';
		}

		/**
		 * Loads the translation files.
		 *
		 * @since  1.0.0
		 * @access public
		 * @return void
		 */
		public function i18n() {
			load_plugin_textdomain( 'wpwgp', false, trailingslashit( dirname( plugin_basename( __FILE__ ) ) ) . 'languages' );
		}

		/**
		 * Loads files needed by the plugin.
		 *
		 * @since  1.0.0
		 * @access private
		 * @return void
		 */
		private function includes() {

			// Load admin files.
			if ( is_admin() ) {

				// General admin functions.
				if ( file_exists( $this->admin_dir . 'class-get-post.php' ) ) {
					require_once $this->admin_dir . 'class-get-post.php';
					$admin_instance = new Admin_Get_Post();// Call Default functions
					$admin_instance->admin_hook();
				}
			}
			// Post link category
			add_filter( 'post_link_category', array( $this, 'post_link_category' ), 10, 3 );

		}

		/**
		 * Filters post_link_category to change the category to the chosen category by the user
		 *
		 * @param stdClass $category The category that is now used for the post link.
		 * @param array    $categories This parameter is not used.
		 * @param WP_Post  $post The post in question.
		 *
		 * @return array|null|object|WP_Error The category we want to use for the post link.
		 */
		public function post_link_category( $category, $categories = null, $post = null ) {
			$post             = get_post( $post );
			$primary_category = get_post_meta( $post->ID, '_wgp_primary_' . $post->post_type, true );

			if ( empty( $primary_category ) ) {
				return $category;
			}

			if ( false !== $primary_category && $primary_category !== $category->cat_ID ) {
				$category = get_category( $primary_category );
			}

			return $category;
		}
	}
endif;

/**
 * Gets the instance of the `Wp_Get_Post` class.  This function is useful for quickly grabbing data
 * used throughout the plugin.
 *
 * @since  1.0.0
 * @access public
 * @return object
 */
function wp_get_post() {
	return Wp_Get_Post::get_instance();
}

// Let's roll!
wp_get_post();
